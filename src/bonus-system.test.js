import {calculateBonuses} from "./bonus-system.js";
const assert = require("assert");


describe('Bonus system tests', () => {
    let app;
    console.log("Tests started");


    test('Valid bonus calculation',  (done) => {
        expect(calculateBonuses("no", 9999)).toBeCloseTo(0.0, 5);
        expect(calculateBonuses("no", 10000)).toBeCloseTo(0.0, 5);
        expect(calculateBonuses("no", 50000)).toBeCloseTo(0.0, 5);
        expect(calculateBonuses("no", 100000)).toBeCloseTo(0.0, 5);

        expect(calculateBonuses("Standard", 9999)).toBeCloseTo(0.05, 5);
        expect(calculateBonuses("Standard", 10000)).toBeCloseTo(0.075, 5);
        expect(calculateBonuses("Standard", 50000)).toBeCloseTo(0.1, 5);
        expect(calculateBonuses("Standard", 100000)).toBeCloseTo(0.125, 5);

        expect(calculateBonuses("Premium", 9999)).toBeCloseTo(0.1, 5);
        expect(calculateBonuses("Premium", 10000)).toBeCloseTo(0.15, 5);
        expect(calculateBonuses("Premium", 50000)).toBeCloseTo(0.2, 5);
        expect(calculateBonuses("Premium", 100000)).toBeCloseTo(0.25, 5);

        expect(calculateBonuses("Diamond", 9999)).toBeCloseTo(0.2, 5);
        expect(calculateBonuses("Diamond", 10000)).toBeCloseTo(0.3, 5);
        expect(calculateBonuses("Diamond", 50000)).toBeCloseTo(0.4, 5);
        expect(calculateBonuses("Diamond", 100000)).toBeCloseTo(0.5, 5);
        done();
    });

    console.log('Tests Finished');

});
